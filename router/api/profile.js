const express = require('express')
const router=express.Router()
const passport =require('passport')
const Profile=require('../../model/profile')

router.get('/test',(req,res)=>{
    res.status(200).json("success")
})

// $route /api/profile/add post
// @ decs 创建信息添加接口
// @res 返回添加的信息
// @access private
router.post('/add',passport.authenticate('jwt',{session:false}),(req,res)=>{
    console.log(req.user,'/')
    const profile={
        income:req.body.income,
        extend:req.body.extend,
        cash:req.body.cash,
    }
    if(req.body.describe){profile.describe=req.body.describe}
    if(req.body.remark){profile.remark=req.body.remark}
    if(req.body.type){profile.type=req.body.type}
    new Profile(profile).save().then((profile)=>{
       res.status(200).json(profile) 
    }).catch(err=>{console.log(err)})
})

// $route /api/profile/ get
// @ decs 获取所有信息
// @res 返回该用户中添加的所有信息
// @access private
router.get('/',passport.authenticate('jwt',{session:false}),(req,res)=>{
    Profile.find().then(profile=>{
        if(!profile){
            res.status(200).json('您没有添加任何信息')
        }else{
            res.status(200).json(profile)
        }
    }).catch(err=>res.status(404).json('请求的资源不存在'))
})
// $route /api/profile/id='' get
// @ decs 获取该用户的指定信息
// @res 返回该用户指定的信息
// @access private
router.get('/:id',passport.authenticate('jwt',{session:false}),(req,res)=>{
    Profile.findOne({_id:req.params.id}).then(profile=>{
        console.log('&',profile)
        res.status(200).json(profile)
    }).catch(err=>res.status(400).json('请求出错，检查id'))
})

// $route /api/profile/edit/:id post 编辑profile信息 bd.Profile.update({},$set:{})
// @ decs 编辑该用户的指定信息
// @res 返回该用户编辑后的信息
// @access private
router.post('/edit/:id',passport.authenticate('jwt',{session:false}),(req,res)=>{
    const newprofile={}
    if(req.body.income){newprofile.income=req.body.income}
    if(req.body.cash){newprofile.cash=req.body.cash}
    if(req.body.extend){newprofile.extend=req.body.extend}
    if(req.body.describe){newprofile.describe=req.body.describe}
    if(req.body.remark){newprofile.remark=req.body.remark}
    if(req.body.type){newprofile.type=req.body.type}
    Profile.findOneAndUpdate({_id:req.params.id},{$set:newprofile},{new:true}).then(profile=>{
        if(!profile){res.status(200).json('不存在该信息，没办法修改')}
        else res.status(200).json(profile)
    }).catch(err=>console.log(err))
})
// $route /api/profile/dele/:id post 编辑profile信息 bd.Profile.update({},$unset:{})
// @ decs 编辑该用户的指定信息
// @res 返回该用户编辑后的信息
// @access private
router.delete('/dele/:id',passport.authenticate('jwt',{session:false}),(req,res)=>{
    Profile.findOneAndRemove({_id:req.params.id}).then(value=>{
        res.status(200).json(value)
    }).catch(err=>{res.status(400).json('删除失败')})
})
module.exports=router