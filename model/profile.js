const mongoose=require('mongoose')
const Schema=mongoose.Schema

const ProfileEtidy=new Schema({
     // Schema作为以文件形式存储的数据库模型骨架，不具备对数据库的操作能力是数据库的抽象
     type:{
        type:String,
    },
    income:{
        type:Number,
        required:true
    },
    cash:{
        type:Number,
        required:true
    },
    date:{
        type:Date,
        default:Date.now
    },
    remark:{
        type:String,
    },
    extend:{
        type:String,
        required:true
    },
    describe:{
        type:String
    }
})

const Profile=mongoose.model('profileInfo',ProfileEtidy)
module.exports=Profile