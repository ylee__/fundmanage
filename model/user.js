const mongoose=require('mongoose')
const Schema=mongoose.Schema

const UserSchema=new Schema({
    // Schema作为以文件形式存储的数据库模型骨架，不具备对数据库的操作能力是数据库的抽象
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    date:{
        type:Date,
        default:Date.now
    },
    avatar:{
        type:String,
        default:''
    },
    identity:{
        type:String,
        required:true
    }
})
const User=mongoose.model("users",UserSchema)
module.exports=User