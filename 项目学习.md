# 数据库连接 D:\ mongod --dbpath D:\deskwork\web_project\data\db --port 5555
# 接口文档（配置后端路由） 
+ 登录&政注册
# 创建数据模型 在model文件夹里
> user集合
> profile集合
+ Schema:它是一种以文件形式存储的数据库模型骨架，不具备对数据库操作的能力，仅仅只是数据库在程序片段中的一种表现，可以理解为表结构。
+ Model:由Schema发布生成的模型，具有抽象属性和行为的数据库操作。
+ Entity:由Model创建的实体，它的操作也会影响数据库。


# 前端路由配置：中的redirect 将'/'中的重定向到'index'

# 请求拦截器和响应拦截器 elLoading.service(option)=拦截效果实例，可以对其进行清除
> axios.interceptors.request.use(config=>{开始拦截效果;return config})
> axios.interceptors.response.use(res=>{结束拦截;return res})

# 前后端分离：前端开启代理服务器，解决跨域问题，然后封装axios进行创造instence实例和进行请求拦截和响应拦截
> 在代理服务器对请求进行转发之前，先查看本地的public下的资源是否存在，如果存在默认则不进行转发，
> 通过devServer：{proxy：’代理服务器‘} 只能配置一个代理

# 在用户登陆之前，用户只能看到login和register页面，只有当用户登陆成功，（即服务器返回了一个代表用户唯一标识的token，
> 全局路由守卫 router.beforeEach((to,from,next)=>{})

# 用户携带收到的token去发送请求，不需要在登陆了，只要这个token不失效
>可以在这里添加全局统一的关卡 比如说token userid等等
+ 判断是否拥有登录有则添加到请求参数中去 也就是 data中去 这样只要请求就会带
+ userid 与token，就不需要再在每个接口中写全局统一的参数

# 重难点
> 考虑到请求失败的时候，对数据的操作，

# 当store中的数据发生改变的时候，需要被组件检测到，那么就需要计算属性，利用计算属性对store中的数据进行检测
> 计算属性geter执行的时机，一上来，以及当所依赖的数据发生变化的时候触发

# 当vue中的页面刷新了，vuex中的数据也重新初始化，会造成页面刷新数据丢失的问题
> 通过使用vuex-persistedstate是vuex的持久化插件，可以将state中的数据，保存在localStorage或sessitonStorage中，
> 使得关闭浏览器或刷新页面数据都不会丢失.

# 客户端携带着token向服务器发送请求的时候，服务端通过passport-jwt对token进行解析验证，失败的话则返回401
> 401即未授权