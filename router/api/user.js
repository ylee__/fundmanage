// login&&Register
const express=require('express')
const router=express.Router()
const bcrypt=require('bcrypt')
const passport =require('passport')
const jwt= require('jsonwebtoken');
const User=require('../../model/user')
const privateKey=require('../../config/mongokeys').privateKey

// 注册接口
// $route   /api/user/rejester
// @res json(用户的注册信息)和status
// @acess public
router.post('/register',(req,res)=>{
    // 校验email的格式，如果错误则返回 xxx@xxx.com
    const emailFormat=/^\w{3,}(\.\w+)*@[A-z0-9]+\.[a-z]{3,5}(\.[a-z]+)*$/
    if(emailFormat.test(req.body.email)){
        User.findOne({email:req.body.email}).then((user)=>{
            if(user){
                res.status(400).json("邮箱已被注册")
            }
            else{
                const newuser=new User({
                    name:req.body.name,
                    email:req.body.email,
                    password:req.body.password,
                    avatar:req.body.avatar,
                    identity:req.body.identity
                })
                bcrypt.genSalt(10, function(err, salt) {
                    bcrypt.hash(newuser.password, salt, function(err, hash) {
                        if(err)throw err
                        newuser.password=hash
                        newuser.save().then((user)=>{res.json({user})})
                                    .catch((reason)=>{console.log(reason)})
                    });
                });
                res.status(200)
            }
        }).catch((reason)=>{
            console.log("heloo ",reason)
        })
    }
    else{
        res.status(400).json("邮箱格式不对")
    }
})

// 登录接口
// $route  /api/user/login
// @desc 用户将用户名或邮箱和密码发送给服务器，服务器现对email进行查找，找到了对密码进行校验，都对上了，则登陆成功
// @res 返回token，用来作为用户的唯一标识，作为用户的通行证
// @acess public
router.post('/login',(req,res)=>{
    const email=req.body.email
    const password=req.body.password
    User.findOne({email}).then(user=>{
        if(!user){res.status(400).json("用户不存在")}
        else{
             bcrypt.compare(password,user.password).then((isMatch)=>{
                if(isMatch){
                    const rule={id:user._id,identity:user.identity,avatar:user.avatar,name:user.name}
                    jwt.sign(rule, privateKey, { expiresIn: 10}, function(err, token) {
                        if(err)throw err//Math.floor(Date.now() / 1000) + (60 * 60)
                        else{ 
                            res.status(200).json({
                                success:true,
                                token:'Bearer '+token
                            })
                        }
                      });
                }
                else{
                    res.status(400).json('密码错误')
                }
             })
        }
       
    })
})

// $route get /api/user/currentUser
// @res 
// @desc 首先验证用户的token
// @acess private
// router.get('/currentUser','验证token',(req,res)=>{
// })
router.get('/currentUser',passport.authenticate('jwt',{session:false}),(req,res)=>{
    res.status(200).json({
        id:req.user.id,
        name:req.user.name,
        email:req.user.email,
        avatar:req.user.avatar,
        identity:req.user.identity
    })
})
module.exports=router