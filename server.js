const express=require('express')
const bodyParser=require('body-parser')
const userRoutes=require('./router/api/user')
const mongoose = require('mongoose');
const passport =require('passport')
const app=express()
const profileRoutes=require('./router/api/profile')

//引入mongoose
const db_url=require('./config/mongokeys').mongoURI
//连接数据库
mongoose.connect(db_url).then((value)=>{
    console.log('数据库连接成功')
}).catch((reason)=>{
    console.log('数据库连接失败')
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))

// passport初始化
app.use(passport.initialize())
require("./config/passport")(passport)      
// 注册接口
app.use('/api/user',userRoutes)
app.use('/api/profile',profileRoutes)
app.listen(5000,()=>{
    console.log('server is Running on 5000')
})