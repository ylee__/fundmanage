//引入mongoose
const mongoose = require('mongoose');
const db_url=require('./config/mongokeys').mongoURI
//1.连接数据库
mongoose.connect(db_url).then((value)=>{
    console.log('数据库连接成功',value)
}).catch((reason)=>{
    console.log('数据库连接失败',reason)
})