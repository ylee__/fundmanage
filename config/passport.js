const JwtStrategy = require('passport-jwt').Strategy,
ExtractJwt = require('passport-jwt').ExtractJwt;
const User=require('../model/user')
const privateKey=require('../config/mongokeys').privateKey

const opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = privateKey;
module.exports=passport=>{
    passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
        console.log(jwt_payload,"jwt中token的信息，是一个对象，包含_id和expiretime和identity等自己配置的信息")
        User.findById(jwt_payload.id).then(user=>{
            if(!user)return done(null,false)
            else{
                return done(null,user)
            }
        }).catch(err=>console.log(err))
    }));
}